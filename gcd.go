package main

import(
	"math/big"
)

/**
  Finding a GCD (Greater Common Divisor)
 **/

func gcd(a *big.Int, b *big.Int) *big.Int {
	zero := big.NewInt(0)
	// On évite les nombres négatifs
	if(a.Cmp(zero) == -1) { a.Mul(a, big.NewInt(-1)) }
	if(b.Cmp(zero) == -1) { a.Mul(b, big.NewInt(-1)) }

	// Si a ou b = 0 => pgcd c'est l'autre
	if(a.Cmp(zero) == 0) { return b }
	if(b.Cmp(zero) == 0) { return a }

	// On dit que a c'est le plus grand !
	if(a.Cmp(b) < 0) {
		a, b = b, a
	}
	// On fait un modulo
	// quotient := a / b
	reste := new(big.Int)
	reste.Mod(a, b)
	// si le reste est nul, on a notre PGCD
	if(reste.Cmp(zero) == 0) {
		return b
	}
	// sinon on fait un appel récursif
	return gcd(b, reste)
}

/**
	Least Common Multiple
	multiply the result of (gcd(a,b) divides a) by b
**/
func lcm(a *big.Int, b *big.Int) *big.Int {
	gcd := gcd(a,b)
	quotient := new(big.Int)
	quotient.Div(a, gcd)
	return big.NewInt(0).Mul(b, quotient)
}
