package main

import(
	"math/big"
)

/** calculate exponation with few multiplications - recursive style
      num^pow = ..
**/
func fastExp(num *big.Int, pow *big.Int) *big.Int {
	powin := new(big.Int)
	zero := big.NewInt(0)
	un := big.NewInt(1)
	deux := big.NewInt(2)

	// Control
	if pow.Cmp(zero) == 0 {
		return new(big.Int).Set(un)
	}
	if pow.Cmp(un) == 0 {
		// Returning pow 1, so num
		return new(big.Int).Set(num)
	}
	if pow.Cmp(deux) == 0 {
		// Returning pow 2
		return new(big.Int).Mul(num, num)
	}

	// Calculation

	// Next recursion
	powin.Div(pow, deux) // pow = pointer, it will be modified for every coming function
	r := fastExp(num, powin) // recursion
	r.Mul(r, r) // square
	
	// Is pow odd ? Then another muliplication
	bigMod := new(big.Int)
	bigMod.Mod(pow, deux)
	if bigMod.Cmp(un) == 0 {
		r.Mul(r, num)
	}
	
	// The end, give the result
	return r
}

/**
 Same with modulo - still recursive style
 num^pow mod mod = ..
**/
func fastExpMod(num *big.Int, pow *big.Int, mod int64) *big.Int {
	powin := new(big.Int)
	bigMod := big.NewInt(mod)
	retour := new(big.Int)
	zero := big.NewInt(0)
	un := big.NewInt(1)
	deux := big.NewInt(2)

	// Control
	if pow.Cmp(zero) == 0 {
		retour.Mod(un, bigMod)
		return retour
	}
	if pow.Cmp(un) == 0 {
		retour.Mod(num, bigMod)
		return retour
	}
	if pow.Cmp(deux) == 0 {
		retour.Mul(num, num)
		retour.Mod(retour, bigMod)
		return retour
	}
	
	// Recursion
	powin.Div(pow, deux)
	r := fastExp(num, powin)
	retour.Mul(r, r)
	retour.Mod(retour, bigMod)
	
	// pow is odd : to multiply by num mod10 + mod10
	bigMod2 := new(big.Int).Mod(pow, deux)
	if bigMod2.Cmp(un) == 0 {
		numMod := new(big.Int)
		numMod.Mod(num, bigMod)
		retour.Mul(retour, numMod)
		retour.Mod(retour, bigMod)
	}
	
	// The end
	return retour
}