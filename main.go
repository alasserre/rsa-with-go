package main

import(
	"log"
	"fmt"
	"math/big"
)

func main() {
	var sa,sb,sc string
	a := new(big.Int)
	b := new(big.Int)
	var mod int64

	// Eternal loop ...
	for {
		fmt.Print("Type three numbers separated by a space for calculating a^b mod c (use 0 as first number to quit): ")
		fmt.Scan(&sa, &sb, &sc)
		// From string to bigInt
    	_, err := fmt.Sscan(sa, a)
		if err != nil {
			log.Println("error scanning value:", err)
			return
		}
		_, err = fmt.Sscan(sb, b)
		if err != nil {
			log.Println("error scanning value:", err)
			return
		}
		_, err = fmt.Sscan(sc, &mod)
		if err != nil {
			log.Println("error scanning value:", err)
			return
		}


		// If < 1 then quit
		if(a.Cmp(big.NewInt(1)) == -1) {
			fmt.Println("Byebye !")
			return	
		} 

		// Sum up and calculate ..
		fmt.Println("Your numbers are:", a, b, "and ", mod)

		resultat1 := fastExp(a, b)
		resultat2 := fastExpMod(a, b, mod)
		fmt.Println("Résultat a^b : ", resultat1)
		fmt.Println("Résultat a^b mod c : ", resultat2)

		/*
		g := gcd(a,b)
		l := lcm(a, b)
		fmt.Println("PGCD : ", g)
		fmt.Println("LCM  : ", l)
		*/
	}
}
